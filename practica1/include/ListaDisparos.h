// Lista.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_)
#define AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_DISPAROS 100

#include "Disparo.h" 

class ListaDisparos { 
	
private: 
		Disparo * lista[MAX_DISPAROS]; 
		int numero; 
public: 
		ListaDisparos(); 
		virtual ~ListaDisparos(); 
		bool Agregar(Disparo *d); 
		void DestruirContenido(); 
		void Mueve(float t); 
		void Dibuja(); 
		bool Colision(Raqueta &r);
		bool Colision1(Raqueta &r);
	
};
#endif // !defined(AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_)

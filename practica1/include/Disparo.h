// Disparo.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_)
#define AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Disparo
{
private:
	unsigned char rojo;
	unsigned char verde;
	unsigned char azul;

	Vector2D origen;
	//Vector2D posicion;
	Vector2D velocidad;
	Vector2D aceleracion;

public:
	Vector2D posicion;
	float radio;

	void Dibuja();
	void Mueve(float t);
	void Set_origen(float x,float y);
	void Set_Pos(float x,float y);
	void Set_Vel(float x,float y);
	bool Rebota(Raqueta &r);
	bool Rebota1(Raqueta &r);
	
	Disparo(void);
	~Disparo(void);
};
#endif // !defined(AFX_RAQUETA_H__D3071D0D_40F1_4C1D_947A_AFD3B0F81E79__INCLUDED_)

// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}

void Esfera::Dibuja()
{
  glColor3ub(centro.x,centro.y,200);//(255,255,0)
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
  centro=centro+velocidad*t+aceleracion*(0.5f*t*t);
  velocidad=velocidad+aceleracion*t;
 
 radio+=0.01f;
 if(radio>1.25)
   radio=0.5f;



}
